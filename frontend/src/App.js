import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link, withRouter, Redirect
} from "react-router-dom";
import Films from "./components/Films/Films";
import Header from "./components/Header/Header";
import Filmssss from "./components/Filmssss/Reservations";
import Film from "./components/Films/Film/Film";
import * as React from "react";

import 'react-responsive-modal/styles.css';
import {Modal} from "react-responsive-modal";

class App extends React.Component {
  state = {
    isLoginModalOpened: false,
    isRegisterModalOpened: false,
    loginConfig: {
      email: null,
      password: null
    },
    registerConfig: {
      name: null,
      surname: null,
      email: null,
      password: null
    }
  }
  openLoginModal = () => {
    this.setState({isLoginModalOpened: true});
  };

  closeLoginModal = () => {
    this.setState({isLoginModalOpened: false});
  };
  films;
  menu = [
    {name: 'Фільми', component: Films, path: '/films'},
    // {name: 'Розклад', component: Filmssss, path: '/timetable'},
    // {name: 'Кінотеатри', component: Filmssss, path: '/theatres'},
  ];

  openRegisterModal = () => {
    this.setState({isRegisterModalOpened: true});
  }

  closeRegisterModal = () => {
    this.setState({isRegisterModalOpened: false});
  }

  login(f) {
    console.log(f);
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(f)
    };

    const url = "http://127.0.0.1:3001/login";
    fetch(url, requestOptions)
      .then(r => r.json())
      .then(r => localStorage.setItem('token', r.token.split(' ')[1]))
      .then(this.closeLoginModal)
      .catch(console.log)
  }

  render() {

    let {name, surname, email, password} = this.state.loginConfig;

    return (
      <div className="App">
        <Router>
          <Header menu={this.menu} openLoginModal={this.openLoginModal} openRegisterModal={this.openRegisterModal}/>
          <Route key='d' path='/films/:filmId' component={Film}/>
          <Route key='dd' path='/films/:filmId/timetables/:timetableId' component={Filmssss} exact/>

          <Switch>
            {this.menu.map(menuItem => <Route key={menuItem.name} path={menuItem.path} exact
                                              component={menuItem.component}/>)}
          </Switch>
          <Route exact path="*">
            <Redirect to="/films"/>
          </Route>
        </Router>
        <Modal open={this.state.isLoginModalOpened} center={true} onClose={this.closeLoginModal}>
          <h2>Login</h2>
          <div>Email<input type='text' value={this.state.loginConfig.email}
                           onChange={(e) => {
                             email = e.target.value;
                             this.setState({loginConfig: {...this.state.loginConfig, email}})
                           }}/>
          </div>
          <div>Password<input type='password' value={this.state.loginConfig.password}
                              onChange={(e) => {
                                password = e.target.value;
                                this.setState({loginConfig: {...this.state.loginConfig, password}})
                              }}/></div>
          <button onClick={() => this.login(this.state.loginConfig)}>Login</button>
        </Modal>
        <Modal open={this.state.isRegisterModalOpened} center={true} onClose={this.closeRegisterModal}>
          <h2>Register</h2>
          <div>Name<input type='text' value={this.state.loginConfig.name}
                          onChange={(e) => {
                            name = e.target.value;
                            this.setState({loginConfig: {...this.state.loginConfig, name}})
                          }
                          }/>
          </div>
          <div>Surname<input type='text' value={this.state.loginConfig.surname}
                             onChange={(e) => {
                               surname = e.target.value;
                               this.setState({loginConfig: {...this.state.loginConfig, surname}})
                             }}/></div>
          <div>Email<input type='text' value={this.state.loginConfig.email}
                           onChange={(e) => {
                             email = e.target.value;
                             this.setState({loginConfig: {...this.state.loginConfig, email}})
                           }}/>
          </div>
          <div>Password<input type='password' value={this.state.loginConfig.password}
                              onChange={(e) => {
                                password = e.target.value;
                                this.setState({loginConfig: {...this.state.loginConfig, password}})
                              }}/></div>
          <button>Register</button>
        </Modal>

      </div>
    );
  }
}

export default withRouter(App);
