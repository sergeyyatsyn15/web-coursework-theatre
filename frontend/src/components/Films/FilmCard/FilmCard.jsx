import './FilmCard.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {NavLink} from "react-router-dom";
import {faHeart as faHeartSolid} from '@fortawesome/free-solid-svg-icons'
import {faHeart as faHeartRegular} from '@fortawesome/free-regular-svg-icons'
import StarRatings from 'react-star-ratings';
import * as React from "react";
import jwt_decode from "jwt-decode";

export default class FilmCard extends React.Component {
  state = {
    userRating: this.props.film.userConfig ? this.props.film.userConfig.ratingFromUser : null
  }

  changeRating = async (value) => {
    const setRatingUrl = "http://localhost:3001/api/favourite-films";

    fetch(setRatingUrl, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        ratingFromUser: value,
        filmId: this.props.film.id,
        userId: jwt_decode(localStorage.getItem('token')).id,
      })
    })
      .then(() => this.setState({userRating: value}));
  }

  likeFilm(liked) {
    const setRatingUrl = "http://localhost:3001/api/favourite-films";

    fetch(setRatingUrl, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        filmId: this.props.film.id,
        userId: jwt_decode(localStorage.getItem('token')).id,
        liked: liked
      })
    })
      .then(() => {
        this.props.film.userConfig.liked = liked;
        this.forceUpdate();
      });
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    if (token) {
      console.log(jwt_decode(token).id);
    }
  }

  render() {
    return (
      <div>
        {this.props.film &&
        <div className="flip-card">
          <div className="flip-card-inner">
            <div className="flip-card-front">
              <div className='movie-top-left-info'>
                <span className='film-type icon'>{this.props.film.type}</span>
                <span className='film-type icon'>{this.props.film.min_age ? this.props.film.min_age : 0}+</span>
              </div>
              <img src={this.props.film.card_img_address} alt="Avatar"/>
            </div>
            <div className="flip-card-back">
              {this.props.film.userConfig && this.props.film.userConfig.liked &&
              <div className='like-icon-wrapper'
                   onClick={() => this.likeFilm(!this.props.film.userConfig.liked)}>
                <FontAwesomeIcon className='like-icon'
                                 icon={this.props.film.userConfig.liked ? faHeartSolid : faHeartRegular} size='lg'/>
              </div>
              }
              {this.state.userRating &&
              <div>
                <StarRatings
                  rating={this.state.userRating}
                  starRatedColor="blue"
                  changeRating={(f) => this.changeRating(f)}
                  numberOfStars={5}
                  name='rating'
                  starDimension='30px'
                />
              </div>
              }
              <p>Rating: {this.props.film.rating}</p>
              <NavLink to={'/films/' + this.props.film.id}>
                <h1 className='film-name'>{this.props.film.name}</h1>
              </NavLink>
              <p>Тривалість: {this.props.film.duration} мин</p>
              {this.props.film &&
              <p>з {this.props.film.release_date}</p>}
            </div>
          </div>
        </div>}
      </div>
    );
  }

}
