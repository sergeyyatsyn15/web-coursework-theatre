import * as React from "react";

import './Films.css'
import FilmList from "./FilmList/FilmList";

class Films extends React.Component {
  state = {
    films: []
  }

  async componentDidMount() {
    const getFilmsUrl = "http://localhost:3001/api/films";
    const filmsResponse = await fetch(getFilmsUrl);
    let films = JSON.parse(await filmsResponse.json());
    this.setState({films: films});

  }

  render() {
    return (
      <div className="headfer">
        {this.state.films && <FilmList films={this.state.films}/>}
      </div>
    );
  }
}

export default Films;
