import './Film.css'
import * as React from "react";
import Reservations from "../../Filmssss/Reservations";

class Film extends React.Component {
  state = {
    film: null,
    timetables: [],
    currentTimetable: null
  };
  days = [
    'Неділя',
    'Понеділок',
    'Вівторок',
    'Середа',
    'Четвер',
    "П'ятниця",
    'Субота'
  ];

  async componentDidMount() {
    const url = "http://localhost:3001/api/films/" + this.props.match.params.filmId;
    const response = await fetch(url);
    const data = await response.json();
    this.state.timetables = data && data.timetables &&
      data.timetables.sort((tt1, tt2) => tt1.startDatetime.localeCompare(tt2.startDatetime))
        .reduce((acc, timetableItem) => {
          let find1 = acc.find(tItem => {
            return tItem.date === timetableItem.startDatetime.split('T')[0];
          });
          if (find1) {
            find1.times.push({start: timetableItem.startDatetime, end: timetableItem.endDatetime});
          } else {
            acc.push({
              id: timetableItem.id,
              reservations: timetableItem.reservations,
              date: timetableItem.startDatetime.split('T')[0],
              times: [
                {start: timetableItem.startDatetime, end: timetableItem.endDatetime}
              ]
            });
          }
          return acc;
        }, []);
    this.setState({film: data})
  }

  render() {

    return (
      <div className=''>
        <div className='centralize'>{this.state.film && this.state.film.name}</div>
        <div className='centralize'>
          <iframe width="560" height="315" src={this.state.film && this.state.film.trailer_address} frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen>
          </iframe>
        </div>
        <div
          className='centralize'>Рік: {this.state.film && this.state.film.rental_from && this.state.film.rental_from.split('-')[0]}</div>
        <div className='centralize'>Країна: {this.state.film && this.state.film.country}</div>
        <div className='centralize'>Тривалість: {this.state.film && this.state.film.duration + ' хвилин'}</div>
        <div className='centralize'>Вікові обмеження: {this.state.film && this.state.film.min_age}+</div>
        {this.state.film && this.state.film.description &&
        <div>
          {this.state.film.description}
        </div>
        }
        <div className='theatre-timeline'>

          {this.state.timetables && this.state.timetables.map((timetable, idx) =>
            <div className={'timetable-item-wrapper-' + idx}>
              <div>{timetable.date} ({this.days[new Date(timetable.date).getDay()]})</div>
              {timetable.times.map(time => <button
                onClick={() => this.setState({currentTimetable: timetable})}>{time.start.slice(11, 16)}</button>)}
            </div>
          )}

        </div>
        {this.state.currentTimetable && <Reservations timetable={this.state.currentTimetable}/>}
      </div>
    );
  }
}

export default Film;
