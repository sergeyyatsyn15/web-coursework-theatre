import './FilmList.css'
import FilmCard from "../FilmCard/FilmCard";

const FilmList = ({films}) => {
  return (
    <ul className='film-list'>
      {films.map(film => <li key={'filmli' + film.id}><FilmCard key={'filmcard' + film.id} film={film}/></li>)}
    </ul>
  );
};

export default FilmList;
