import './Reservations.css'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import * as React from "react";
import jwt_decode from "jwt-decode";

class Reservations extends React.Component {
  state = {}
  a = {taken: false};
  b = {taken: true};
  theatre = {
    rowSize: 20,
    colSize: 20,
    seats: new Array(20 * 20).fill(0).map((value, index) => this.a)
  }
  containerColumnStyle = new Array(this.theatre.rowSize).fill(0).map(() => '20px').join(' ');
  containerRowStyle = new Array(this.theatre.colSize).fill(0).map(() => '20px').join(' ');

  componentDidMount() {
    this.props.timetable.reservations.forEach(reservation => this.theatre.seats[Number(reservation.seat)] = this.b);
  }

  async makeReservation(index) {
    const url = "http://127.0.0.1:3001/api/reservations";
    const respose = await fetch(url, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        time_table_id: this.props.timetable.id,
        user_id: jwt_decode(localStorage.getItem('token')).id,
        seat: index
      })
    })
    console.log(respose)
  }

  render() {
    return (
      <div className="headfer">
        {this.theatre.seats && this.theatre.seats.length &&
        <div className='container'
             style={{gridTemplateColumns: this.containerColumnStyle, gridTemplateRows: this.containerRowStyle}}>
          {this.theatre.seats.map((seat, index) => {
            return seat.taken ?
              <div className='seat-wrapper'>
                <FontAwesomeIcon className='like-icon' icon={faTimes} size='lg' color='grey'/>
              </div> :
              <div className='seat-wrapper' onClick={() => this.makeReservation(index)}><span
                className='free-seat-icon'/></div>
          })
          }
        </div>
        }
      </div>
    );
  }
}

export default Reservations;
