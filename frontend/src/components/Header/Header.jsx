import './Header.css'
import {Link, NavLink} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSignInAlt, faSignOutAlt} from "@fortawesome/free-solid-svg-icons";
import * as React from "react";
import jwt_decode from "jwt-decode";

class Header extends React.Component {
  state = {
    name: 'Бумеранг не запущен'
  };

  updateData = (value) => {
    this.setState({name: value})
  }

  render() {
    console.log(this.props)
    return (
      <div className="header">
        <nav>
          <ul className={'nav-list'}>
            {this.props.menu.map(menuItem =>
              <NavLink to={menuItem.path} key={'item-' + menuItem.name}
                       className={'nav-list__item '} activeClassName='active'>{menuItem.name}</NavLink>
            )}
            {!localStorage.getItem('token') &&
            <div className='cursor-pointer' onClick={this.props.openLoginModal}><FontAwesomeIcon icon={faSignInAlt} color='white'/></div>}
            {!localStorage.getItem('token') &&
            <div className='cursor-pointer' onClick={this.props.openRegisterModal}><FontAwesomeIcon icon={faSignOutAlt} color='white'/>
            </div>}
            {localStorage.getItem('token') &&
            <div>{jwt_decode(localStorage.getItem('token')).name} {jwt_decode(localStorage.getItem('token')).surname}</div>}
            {localStorage.getItem('token') &&
            <div className='cursor-pointer' onClick={() => {localStorage.removeItem('token'); this.forceUpdate()}}><FontAwesomeIcon icon={faSignOutAlt} color='white'/>
            </div>}
          </ul>
        </nav>
      </div>
    );
  }

}


export default Header;
