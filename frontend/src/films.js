export let films = [
  {
    name: 'Афера по-українські',
    minAge: 18,
    img: 'https://planetakino.ua/res/get-poster/00000000000000000000000000002792/640/944/vend10.jpg',
    releaseDate: '2020-10-10',
    trailer: '',
    description: '',
    country: '',
    genres: ['az', 'asf'],
    languages: ['Українська, Російська'],
    rentalFrom: '',
    rentalTo: '',
    duration: 100,
    type: '2D',
    rating: 4.5,
    userInfo: {
      liked: false, //?
      rating: 4
    }
  }
]
