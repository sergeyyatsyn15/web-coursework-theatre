const express = require("express");
const Sequelize = require("sequelize");
var jwt = require('jsonwebtoken');
// var bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const bcrypt = require('bcrypt');

dotenv.config();

const app = express();
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use(express.json());

const sequelize = new Sequelize('theatre', 'postgres', 'postgres', {
  host: 'localhost',
  dialect: 'postgres'
})

const filmTable = sequelize.define('film', {
  name: Sequelize.STRING(255),
  cardImgAddress: Sequelize.TEXT,
  releaseDate: Sequelize.DATE,
  trailerAddress: Sequelize.TEXT,
  description: Sequelize.TEXT,
  minAge: Sequelize.INTEGER,
  country: Sequelize.TEXT,
  rentalFrom: Sequelize.DATE,
  rentalTo: Sequelize.DATE,
  duration: Sequelize.INTEGER,
  rating: Sequelize.DOUBLE,
  type: Sequelize.STRING(50),
}, {freezeTableName: true});

const filmScreenshotTable = sequelize.define('film_screenshots', {
  filmId: Sequelize.BIGINT,
  url: Sequelize.TEXT,
}, {freezeTableName: true});

filmTable.hasMany(filmScreenshotTable, {as: 'screenshots', foreignKey: 'filmId'})
filmScreenshotTable.belongsTo(filmTable, {as: 'screenshots', foreignKey: 'filmId'})

const timeTable = sequelize.define('timetables', {
  filmId: Sequelize.BIGINT,
  startDatetime: Sequelize.DATE,
  endDatetime: Sequelize.DATE,
}, {freezeTableName: true});

filmTable.hasMany(timeTable, {as: 'timetables', foreignKey: 'filmId'})
timeTable.belongsTo(filmTable, {as: 'timetables', foreignKey: 'filmId'})

const userTable = sequelize.define('users', {
  name: Sequelize.TEXT,
  surname: Sequelize.TEXT,
  image: Sequelize.BLOB,
  password: Sequelize.TEXT,
  balance: Sequelize.INTEGER,
  role: Sequelize.TEXT,
  email: Sequelize.TEXT,
}, {freezeTableName: true});

const favouriteFilmTable = sequelize.define('favourite_films', {
  filmId: Sequelize.BIGINT,
  userId: Sequelize.BIGINT,
  ratingFromUser: Sequelize.BIGINT,
  liked: Sequelize.BOOLEAN
}, {freezeTableName: true});

userTable.hasMany(favouriteFilmTable, {as: 'favourite', foreignKey: 'userId'})
favouriteFilmTable.belongsTo(userTable, {as: 'favourite', foreignKey: 'userId',})

const reservationsTable = sequelize.define('reservations', {
  time_table_id: Sequelize.BIGINT,
  user_id: Sequelize.BIGINT,
  seat: Sequelize.BIGINT
}, {freezeTableName: true});

timeTable.hasMany(reservationsTable, {as: 'reservations', foreignKey: 'time_table_id'})
reservationsTable.belongsTo(timeTable, {as: 'reservations', foreignKey: 'time_table_id',})

const tables = {
  Film: filmTable,
  FilmScreenshot: filmScreenshotTable,
  Timetable: timeTable,
  User: userTable,
  FavouriteFilm: favouriteFilmTable,
  Reservation: reservationsTable
}

const generateJWTToken = (userData) => {
  return jwt.sign(userData, process.env.SECRET_KEY);
}
const verifyToken = (jwtToken) => {
  try {
    return jwt.verify(jwtToken, process.env.SECRET_KEY);
  } catch (e) {
    console.log('e:', e);
    return null;
  }
}

async function main() {
  await sequelize.sync();

  app.get("/api/films", async (request, response) => {
    const data = await filmTable.findAll(
      {
        attributes: ['id', 'name', 'card_img_address', 'release_date', 'trailer_address',
          'description', 'min_age', 'country', 'rental_from', 'rental_to', 'duration', 'rating', 'type'],
        include: {
          model: tables.Timetable,
          as: 'timetables',
          order: [['startDatetime', 'ASC']]
        },

      });
    response.json(JSON.stringify(data.map(d => d.dataValues)))
  });

  app.get("/api/films/:filmId", async (request, response) => {
    const data = await filmTable.findOne(
      {
        attributes: ['id', 'name', 'card_img_address', 'release_date', 'trailer_address',
          'description', 'min_age', 'country', 'rental_from', 'rental_to', 'duration', 'rating', 'type'],
        where: {id: request.params.filmId},
        include: [
          {
            model: filmScreenshotTable,
            as: 'screenshots'
          },
          {
            model: timeTable,
            as: 'timetables',
            include: [
              {
                model: reservationsTable,
                as: 'reservations',
              }
            ]
          }
        ]
      });
    response.json(data.dataValues)
  });

  app.get("/api/users", async (request, response) => {
    const data = await tables.User.findAll(
      {
        attributes: ['id', 'name', 'surname', 'image', 'password', 'balance', 'role', 'email'],
        include: [
          {
            model: favouriteFilmTable,
            as: 'favourite'
          }
        ]
      });
    response.json(JSON.stringify(data))
  });

  app.get("/api/users/:userId", async (request, response) => {
    const user = await tables.User.findOne({
      where: {
        id: request.params.userId
      },
      include: [
        {
          model: favouriteFilmTable,
          as: 'favourite'
        }
      ]
    });
    user.password = undefined;
    response.end(JSON.stringify(user.dataValues));
  });

  app.post("/api/users", async (request, response) => {
    console.log(request.body);
    const model = await tables.User.create({
      name: request.body.name,
      surname: request.body.surname,
      password: request.body.password,
      role: 'MENTOR',
      email: request.body.email
    });
    response.json(JSON.stringify(model))
    response.end()
  });

  app.post('/login', (async (req, res) => {
    if (req.body) {
      const userByEmail = await tables.User.findOne({where: {email: req.body.email}});
      if (userByEmail) {
        console.log(userByEmail.dataValues);
        if (bcrypt.compareSync(req.body.password, userByEmail.dataValues.password)) {
          var token = jwt.sign({
            id: userByEmail.dataValues.id,
            name: userByEmail.dataValues.name,
            surname: userByEmail.dataValues.surname,
            email: userByEmail.dataValues.email
          }, process.env.SECRET_KEY.replace(/\\n/gm, '\n'));
        }
        if (token) {
          res.setHeader('authentication', 'Bearer ' + token);
          res.json({token: 'Bearer ' + token});
        } else {
          console.log('NONE')
        }
      }
    }
    res.end();
  }))

  app.post("/api/favourite-films", async (request, response) => {
    console.log(request.body);
    if (request.body && request.body.filmId && request.body.userId && (request.body.ratingFromUser || request.body.liked !== null)) {
      tables.FavouriteFilm.findOne({
        where: {
          filmId: request.body.filmId,
          userId: request.body.userId
        }
      }).then((favouriteFilm) => {
        if (favouriteFilm) {
          const keys = {
            filmId: request.body.filmId,
            userId: request.body.userId,
          };
          if (request.body.ratingFromUser) keys.ratingFromUser = request.body.ratingFromUser;
          if (request.body.liked != null) keys.liked = request.body.liked;
          favouriteFilm.update(keys).then(() => response.end())
        } else {
          tables.FavouriteFilm.create({
            filmId: request.body.filmId,
            userId: request.body.userId,
            ratingFromUser: request.body.ratingFromUser,
            liked: request.body.liked
          })
        }
      }).then(() => response.end())
    }
  });

  app.post("/api/reservations", async (request, response) => {
    let model;
    if (request.body && request.body.seat && request.body.user_id && request.body.time_table_id) {
      model = await tables.Reservation.create({
        time_table_id: request.body.time_table_id,
        user_id: request.body.user_id,
        seat: request.body.seat,
      }).catch(console.log);
      console.log('asdfdasf');
      response.send(model)
    } else {
      response.status(400);
      response.end();
    }
  });

  app.listen(3001, '127.0.0.1');
}

main();
